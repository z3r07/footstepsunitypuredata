﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFollow : MonoBehaviour {

    public Transform playerTransform;

    private Vector3 cameraOffset;

    [Range(0.01f, 10.0f)]
    public float smoothFactor = 1f;
    [Range(1f, 5f)]
    public float rotationSpeed = 1f;


    //Copiare i metodi e void OnEnable() e OnDisable() in PlayerController

    void Start() {
        cameraOffset = transform.position - playerTransform.position;
    }

    void LateUpdate() {

        Quaternion camTurnAngle = Quaternion.AngleAxis(Input.GetAxis("Horizontal") * rotationSpeed, Vector3.up);
        cameraOffset = camTurnAngle * cameraOffset;

        Vector3 newPos = playerTransform.position + cameraOffset;

        transform.position = Vector3.Slerp(transform.position, newPos, smoothFactor);

        transform.LookAt(playerTransform);
    }
}
