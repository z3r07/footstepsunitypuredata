﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using LibPDBinding;

public class PlayerController : MonoBehaviour {

    [Range(1f, 10f)]
    public float speed = 2f;

    public bool isRunning = false;

    public bool walkValueSent = false;
    public bool stopValueSent = false;
    public bool runValueSent = false;

    private Rigidbody rigidbody;
    private Animator animator;

    void Awake() {
        rigidbody = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
    }

    void FixedUpdate() {

        float verticalMovement = Input.GetAxis("Vertical");

        transform.Rotate(0, Input.GetAxis("Horizontal") * 2f, 0);

        if(Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) {
            isRunning = true;
            speed = 5f;
        } else {
            isRunning = false;
            speed = 2f;
        }

        Vector3 camForward_Dir = Vector3.Scale(Camera.main.transform.forward, new Vector3(1, 0, 1)).normalized;
        Vector3 movement = verticalMovement * camForward_Dir;

        if(movement.magnitude > 1f) movement.Normalize();
        movement = movement * speed;

        if(Input.GetKey(KeyCode.W)) {
            if(isRunning) {
                animator.SetBool("IsIdle", false);
                animator.SetBool("IsWalking", false);
                animator.SetBool("IsRunning", true);
                rigidbody.MovePosition(transform.position + movement * Time.deltaTime);
                if(!runValueSent) {
                    LibPD.SendFloat("velocita", 0.25f);
                    runValueSent = true;
                    walkValueSent = false;
                    stopValueSent = false;
                }
                
            } else {
                animator.SetBool("IsIdle", false);
                animator.SetBool("IsWalking", true);
                animator.SetBool("IsRunning", false);
                rigidbody.MovePosition(transform.position + movement * Time.deltaTime);
                if(!walkValueSent) {
                    LibPD.SendFloat("velocita", 0.10f);
                    walkValueSent = true;
                    stopValueSent = false;
                    runValueSent = false;
                }
            }
        } else {
            animator.SetBool("IsIdle", true);
            animator.SetBool("IsWalking", false);
            animator.SetBool("IsRunning", false);
            rigidbody.MovePosition(transform.position + Vector3.zero);
            if(!stopValueSent) {
                LibPD.SendFloat("velocita", 0f);
                stopValueSent = true;
                walkValueSent = false;
                runValueSent = false;

            }
        }
    }


    void OnTriggerEnter(Collider other) {
        switch(other.gameObject.name) {
            case "GravelPlane":
                //Inviare messaggio PD Gravel
                LibPD.SendMessage("gh", "1");
                break;
            case "WoodPlane":
                //Inviare messaggio PD Wood
                LibPD.SendMessage("le", "1");
                break;
            case "MetalPlane":
                //Inviare messaggio PD Metal
                LibPD.SendMessage("me", "1");
                break;
        }
    }


}
