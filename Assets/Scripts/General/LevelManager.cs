﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour {

    public GameObject form;
    public GameObject fps;
    public Level level;

    public List<Toggle> materials = new List<Toggle>();
    private List<string> nameMaterials = new List<string>();
    
    public Toggle value1;
    public Toggle value2;
    public Toggle value3;
    public Toggle value4;
    public Toggle value5;
    public Toggle value6;
    public Toggle value7;

    private string materialSelected;
    private string valueSelected;

    private bool isWrite = false;

    void Awake() {
        nameMaterials.Add("Ghiaia");
        nameMaterials.Add("Legno");
        nameMaterials.Add("Metallo");
        nameMaterials.Add("Neve");
        nameMaterials.Add("Sabbia");
        nameMaterials.Add("Vetro");
        nameMaterials.Add("Asfalto");
        nameMaterials.Add("Ceramica");
        nameMaterials.Add("Marmo");

        foreach(Toggle mat in materials) {
            int index = Random.Range(0, nameMaterials.Count);
            mat.transform.GetChild(2).GetComponent<Text>().text = nameMaterials[index];
            nameMaterials.RemoveAt(index);
        }
    }

    void Start() {
        Cursor.visible = false;
        Cursor.lockState = Cursor.lockState = CursorLockMode.None;

        value1.isOn = false;
        value2.isOn = false;
        value3.isOn = false;
        value4.isOn = false;
        value5.isOn = false;
        value6.isOn = false;
        value7.isOn = false;
    }

    void Update () {
        if(Input.GetKey(KeyCode.Return)) {
            if(GameObject.FindGameObjectWithTag("Player").GetComponent<FirstPersonController>().isProcedural)
                Camera.main.gameObject.GetComponent<LibPdFilterRead>().enabled = false;
            fps.GetComponent<FirstPersonController>().m_MouseLook.SetCursorLock(false);
            fps.GetComponent<FirstPersonController>().enabled = false;
            GetComponent<Timer>().enabled = false;
            form.SetActive(true);
        }

        if(form.activeSelf) {
            //0
            if(materials[0].isOn) {
                materials[1].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[2].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[3].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[4].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[5].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[6].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[7].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[8].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[1].isOn = false;
                materials[2].isOn = false;
                materials[3].isOn = false;
                materials[4].isOn = false;
                materials[5].isOn = false;
                materials[6].isOn = false;
                materials[7].isOn = false;
                materials[8].isOn = false;
                materials[0].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(true);
                materialSelected = materials[0].transform.GetChild(2).GetComponent<Text>().text;
            }
            //1
            if(materials[1].isOn) {
                materials[0].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[2].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[3].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[4].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[5].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[6].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[7].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[8].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[0].isOn = false;
                materials[2].isOn = false;
                materials[3].isOn = false;
                materials[4].isOn = false;
                materials[5].isOn = false;
                materials[6].isOn = false;
                materials[7].isOn = false;
                materials[8].isOn = false;
                materials[1].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(true);
                materialSelected = materials[1].transform.GetChild(2).GetComponent<Text>().text;
            }
            //2
            if(materials[2].isOn) {
                materials[1].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[0].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[3].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[4].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[5].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[6].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[7].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[8].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[1].isOn = false;
                materials[0].isOn = false;
                materials[3].isOn = false;
                materials[4].isOn = false;
                materials[5].isOn = false;
                materials[6].isOn = false;
                materials[7].isOn = false;
                materials[8].isOn = false;
                materials[2].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(true);
                materialSelected = materials[2].transform.GetChild(2).GetComponent<Text>().text;
            }
            //3
            if(materials[3].isOn) {
                materials[1].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[2].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[0].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[4].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[5].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[6].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[7].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[8].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[1].isOn = false;
                materials[2].isOn = false;
                materials[0].isOn = false;
                materials[4].isOn = false;
                materials[5].isOn = false;
                materials[6].isOn = false;
                materials[7].isOn = false;
                materials[8].isOn = false;
                materials[3].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(true);
                materialSelected = materials[3].transform.GetChild(2).GetComponent<Text>().text;
            }
            //4
            if(materials[4].isOn) {
                materials[1].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[2].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[3].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[0].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[5].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[6].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[7].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[8].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[1].isOn = false;
                materials[2].isOn = false;
                materials[3].isOn = false;
                materials[0].isOn = false;
                materials[5].isOn = false;
                materials[6].isOn = false;
                materials[7].isOn = false;
                materials[8].isOn = false;
                materials[4].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(true);
                materialSelected = materials[4].transform.GetChild(2).GetComponent<Text>().text;
            }
            //5
            if(materials[5].isOn) {
                materials[1].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[2].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[3].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[4].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[0].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[6].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[7].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[8].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[1].isOn = false;
                materials[2].isOn = false;
                materials[3].isOn = false;
                materials[4].isOn = false;
                materials[0].isOn = false;
                materials[6].isOn = false;
                materials[7].isOn = false;
                materials[8].isOn = false;
                materials[5].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(true);
                materialSelected = materials[5].transform.GetChild(2).GetComponent<Text>().text;
            }
            //6
            if(materials[6].isOn) {
                materials[1].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[2].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[3].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[4].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[5].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[0].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[7].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[8].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[1].isOn = false;
                materials[2].isOn = false;
                materials[3].isOn = false;
                materials[4].isOn = false;
                materials[5].isOn = false;
                materials[0].isOn = false;
                materials[7].isOn = false;
                materials[8].isOn = false;
                materials[6].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(true);
                materialSelected = materials[6].transform.GetChild(2).GetComponent<Text>().text;
            }
            //7
            if(materials[7].isOn) {
                materials[1].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[2].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[3].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[4].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[5].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[6].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[0].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[8].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[1].isOn = false;
                materials[2].isOn = false;
                materials[3].isOn = false;
                materials[4].isOn = false;
                materials[5].isOn = false;
                materials[6].isOn = false;
                materials[0].isOn = false;
                materials[8].isOn = false;
                materials[7].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(true);
                materialSelected = materials[7].transform.GetChild(2).GetComponent<Text>().text;
            }
            //8
            if(materials[8].isOn) {
                materials[1].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[2].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[3].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[4].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[5].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[6].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[7].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[0].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                materials[1].isOn = false;
                materials[2].isOn = false;
                materials[3].isOn = false;
                materials[4].isOn = false;
                materials[5].isOn = false;
                materials[6].isOn = false;
                materials[7].isOn = false;
                materials[0].isOn = false;
                materials[8].transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(true);
                materialSelected = materials[8].transform.GetChild(2).GetComponent<Text>().text;
            }


            if(value1.isOn) {
                value2.isOn = false;
                value3.isOn = false;
                value4.isOn = false;
                value5.isOn = false;
                value6.isOn = false;
                value7.isOn = false;
                value1.transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                value1.transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(true);
                valueSelected = value1.transform.GetChild(2).GetComponent<Text>().text;
            }
            if(value2.isOn) {
                value1.isOn = false;
                value3.isOn = false;
                value4.isOn = false;
                value5.isOn = false;
                value6.isOn = false;
                value7.isOn = false;
                value2.transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                value2.transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(true);
                valueSelected = value2.transform.GetChild(2).GetComponent<Text>().text;
            }
            if(value3.isOn) {
                value2.isOn = false;
                value1.isOn = false;
                value4.isOn = false;
                value5.isOn = false;
                value6.isOn = false;
                value7.isOn = false;
                value3.transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                value3.transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(true);
                valueSelected = value3.transform.GetChild(2).GetComponent<Text>().text;
            }
            if(value4.isOn) {
                value2.isOn = false;
                value3.isOn = false;
                value1.isOn = false;
                value5.isOn = false;
                value6.isOn = false;
                value7.isOn = false;
                value4.transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                value4.transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(true);
                valueSelected = value4.transform.GetChild(2).GetComponent<Text>().text;
            }
            if(value5.isOn) {
                value2.isOn = false;
                value3.isOn = false;
                value4.isOn = false;
                value1.isOn = false;
                value6.isOn = false;
                value7.isOn = false;
                value5.transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                value5.transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(true);
                valueSelected = value5.transform.GetChild(2).GetComponent<Text>().text;
            }
            if(value6.isOn) {
                value2.isOn = false;
                value3.isOn = false;
                value4.isOn = false;
                value5.isOn = false;
                value1.isOn = false;
                value7.isOn = false;
                value6.transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                value6.transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(true);
                valueSelected = value6.transform.GetChild(2).GetComponent<Text>().text;
            }
            if(value7.isOn) {
                value2.isOn = false;
                value3.isOn = false;
                value4.isOn = false;
                value5.isOn = false;
                value6.isOn = false;
                value1.isOn = false;
                value7.transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(false);
                value7.transform.GetChild(1).GetComponent<Image>().gameObject.SetActive(true);
                valueSelected = value7.transform.GetChild(2).GetComponent<Text>().text;
            }
        }
  
    }

    public void WriteResult() {
        string path = Application.dataPath + "/RisultatiTest.txt";
        if(!File.Exists(path)) {
            File.WriteAllText(path, "---------------------------------------------\n");
        }

        string content = "Level ID: " + level.id.ToString() + "\n" +
                         "Level Name: " + level.nameLevel.ToString() + "\n" +
                         "Minutes: " + level.minutes.ToString() + "\n" +
                         "Seconds: " + level.seconds.ToString() + "\n" +
                         "Material: " + materialSelected + "\n" +
                         "Rating: " + valueSelected + "\n" +
                         "---------------------------------------------\n";

        File.AppendAllText(path, content);

        int i = GameManager.Instance.GetNextSceneId();

        GameManager.Instance.NextScene(i);
    }
 
}
