﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Collections;

public class CreateFile : MonoBehaviour {

    public Toggle maleToggle;
    public Toggle femaleToggle;
    public InputField ageInputField;
    public Toggle siToggle;
    public Toggle noToggle;

    private string sex;
    private string expert;

    void Start() {
        maleToggle.isOn = false;
        femaleToggle.isOn = false;
        siToggle.isOn = false;
        noToggle.isOn = false;
    }

    void Update() {
        if(maleToggle.isOn) {
            femaleToggle.isOn = false;
            maleToggle.transform.GetChild(1).GetComponent<Image>().enabled = true;
            sex = maleToggle.name.ToString();
        }
        if(femaleToggle.isOn) {
            maleToggle.isOn = false;
            femaleToggle.transform.GetChild(1).GetComponent<Image>().enabled = true;
            sex = femaleToggle.name.ToString();
        }
        if(siToggle.isOn) {
            noToggle.isOn = false;
            siToggle.transform.GetChild(1).GetComponent<Image>().enabled = true;
            expert = siToggle.name.ToString();
        }
        if(noToggle.isOn) {
            siToggle.isOn = false;
            noToggle.transform.GetChild(1).GetComponent<Image>().enabled = true;
            expert = noToggle.name.ToString();
        }

    }

    public void WriteFile() {
        string path = Application.dataPath + "/RisultatiTest.txt";
        if(!File.Exists(path)) {
            File.WriteAllText(path, "---------------------------------------------\n");
        }

        string content = "Sesso: " + sex + "\n" +
                         "Eta': " + ageInputField.text + "\n" +
                         "Esperto: " + expert + "\n" +
                         "---------------------------------------------\n";

        File.AppendAllText(path, content);
    }
}
