﻿using UnityEngine;
using System.Collections;


[CreateAssetMenu(fileName = "Level", menuName = "Level", order = 1)]
public class Level : ScriptableObject {
    public int id;
    public string nameLevel;
    public int count;
    public int rate;
    public float minutes;
    public float seconds;
}
