﻿using UnityEngine;
using System.Collections;

public class EndTest : MonoBehaviour {

	void Start () {
        StartCoroutine(Exit(2f));
	}

    private IEnumerator Exit(float time) {
        yield return new WaitForSeconds(time);
        Application.Quit();
    }
}
