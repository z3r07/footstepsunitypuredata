﻿using UnityEngine;
using System.Collections;

public class Timer : MonoBehaviour {

    public Level level;

    private float startTimer;
    private float minutes;
    private float seconds;

	// Use this for initialization
	void Start () {
        startTimer = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
        float t = Time.time - startTimer;

        minutes = (int)t / 60;
        seconds = t % 60;

        level.minutes = minutes;
        level.seconds = seconds;
    }
}
