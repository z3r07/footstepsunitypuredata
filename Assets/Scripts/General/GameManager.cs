﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    
    public static GameManager Instance = null;
    public List<Level> scenes;
    public List<Level> testScenes;

    public int testIndex = 0;

    void Awake() {
        if(Instance == null)
            Instance = this;
        else if(Instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }

    void Start() {
        StartCoroutine(ResetCounter());
        StartCoroutine(GenerateLevels());
    }

    public void StartTest() {
        GameObject.FindGameObjectWithTag("CreateFile").GetComponent<CreateFile>().WriteFile();
        SceneManager.LoadScene(testScenes[testIndex].id);
    }

    public void NextScene(int id) {
        SceneManager.LoadScene(id);
    }

    private IEnumerator ResetCounter() {
        foreach(Level scene in scenes) {
            scene.count = 0;
            scene.seconds = 0f;
            scene.minutes = 0f;
            scene.rate = 0;
        }
        yield return null;
    }

    private IEnumerator GenerateLevels() {
        while(scenes.Count > 0) {
            int index = Random.Range(0, scenes.Count);
            if(scenes[index].count < 3) {
                scenes[index].count += 1;
                testScenes.Add(scenes[index]);
                if(scenes[index].count == 3)
                    scenes.RemoveAt(index);
            }
        }
        yield return null;
    }
    
    public int GetNextSceneId() {
        testIndex++;
        if(testIndex < 18)
            return testScenes[testIndex].id;
        else
            return 7;
    }

}
